/* -*- Mode: C; c-set-style: gnu indent-tabs-mode: t; c-basic-offset: 4; tab-width: 8 -*- */
/*
 * Copyright (C) 2000 SuSE GmbH
 * Author: Martin Baulig <baulig@suse.de>
 *
 * This file is part of GTop.
 *
 * GTop is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * GTop is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with GTop; see the file COPYING.  If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GTOP_LIBGTOP_CLIENT_PRIVATE_H__
#define __GTOP_LIBGTOP_CLIENT_PRIVATE_H__

#include "gtop-libgtop-client.h"

BEGIN_GNOME_DECLS

typedef void (*GTopLibGTopTimerFunc)            (GTopLibGTopClient *,
                                                 gpointer);

typedef struct _GTopLibGTopTimer  		GTopLibGTopTimer;

guint
_gtop_libgtop_client_register_timer            (GTopLibGTopClient *glc,
                                                GTopLibGTopTimerFunc func,
						GTopLibGTopDataType hook_type,
                                                gulong interval,
                                                gpointer data);

void
_gtop_libgtop_client_unregister_timer          (GTopLibGTopClient *glc,
                                                guint timer_id);

void
_gtop_libgtop_client_start_all_timers          (GTopLibGTopClient *glc);

void
_gtop_libgtop_client_stop_all_timers           (GTopLibGTopClient *glc);

void
_gtop_libgtop_client_register_private_timers   (GTopLibGTopClient *glc,
						gpointer user_data);

void
_gtop_libgtop_client_unregister_private_timers (GTopLibGTopClient *glc);

END_GNOME_DECLS

#endif /* __GTOP_LIBGTOP_CLIENT_H__ */

