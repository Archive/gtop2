/* -*- Mode: C; c-set-style: gnu indent-tabs-mode: t; c-basic-offset: 4; tab-width: 8 -*- */
/*
 * Copyright (C) 2000 SuSE GmbH
 * Author: Martin Baulig <baulig@suse.de>
 *
 * This file is part of GTop.
 *
 * GTop is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * GTop is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with GTop; see the file COPYING.  If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gtop-libgtop-client-private.h"

#define MIN_UPDATE_INTERVAL	50L

static void gtop_libgtop_client_class_init          (GTopLibGTopClientClass *class);
static void gtop_libgtop_client_init                (GTopLibGTopClient      *selector);
static void gtop_libgtop_client_destroy             (GtkObject        *object);
static void gtop_libgtop_client_finalize            (GtkObject        *object);

static GtkObject *parent_class;

static guint last_timer_id = 0;
static guint last_hook_id = 0;

struct _GTopLibGTopUpdateData {
    guint id;
    GTopLibGTopData *data;
    GTopLibGTopUpdateFunc func;
    gpointer user_data;
};

struct _GTopLibGTopTimer
{
    GTopLibGTopClient *glc;

    GTopLibGTopDataType hook_type;

    guint id;
    gulong interval;
    guint timer_tag;

    GTopLibGTopTimerFunc func;
    gpointer data;
};

struct _GTopLibGTopClientPrivate
{
    GConfClient *client;

    GTopLibGTopData *data;
    GPtrArray *update_hooks [GTOP_LIBGTOP_DATA_TYPE__MAX];

    GSList *timer_list;
    GArray *private_timer_ids;

    glibtop *server;
};

guint
gtop_libgtop_client_get_type (void)
{
    static guint gtop_libgtop_client_type = 0;

    if (!gtop_libgtop_client_type) {
	GtkTypeInfo gtop_libgtop_client_info = {
	    "GTopLibGTopClient",
	    sizeof (GTopLibGTopClient),
	    sizeof (GTopLibGTopClientClass),
	    (GtkClassInitFunc) gtop_libgtop_client_class_init,
	    (GtkObjectInitFunc) gtop_libgtop_client_init,
	    NULL,
	    NULL,
	    NULL
	};

	gtop_libgtop_client_type = gtk_type_unique (gtk_object_get_type (),
						    &gtop_libgtop_client_info);
    }

    return gtop_libgtop_client_type;
}

static void
gtop_libgtop_client_class_init (GTopLibGTopClientClass *class)
{
    GtkObjectClass *object_class;

    object_class = (GtkObjectClass *) class;

    parent_class = gtk_type_class (gtk_object_get_type ());

    object_class->destroy = gtop_libgtop_client_destroy;
    object_class->finalize = gtop_libgtop_client_finalize;
}

static void
gtop_libgtop_client_init (GTopLibGTopClient *glc)
{
    glc->_priv = g_new0 (GTopLibGTopClientPrivate, 1);
    glc->_priv->data = g_new0 (GTopLibGTopData, 1);
}

static void
gtop_libgtop_client_destroy (GtkObject *object)
{
    GTopLibGTopClient *glc;

    /* remember, destroy can be run multiple times! */

    g_return_if_fail (object != NULL);
    g_return_if_fail (GTOP_IS_LIBGTOP_CLIENT (object));

    glc = GTOP_LIBGTOP_CLIENT (object);

    _gtop_libgtop_client_unregister_private_timers (glc);

    if (GTK_OBJECT_CLASS (parent_class)->destroy)
	(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gtop_libgtop_client_finalize (GtkObject *object)
{
    GTopLibGTopClient *glc;

    g_return_if_fail (object != NULL);
    g_return_if_fail (GTOP_IS_LIBGTOP_CLIENT (object));

    glc = GTOP_LIBGTOP_CLIENT (object);

    if (glc->_priv) {
	guint i;

	gtk_object_unref (GTK_OBJECT (glc->_priv->client));
	/* glibtop_server_unref (glc->_priv->server); */

	g_free ((gpointer) glc->_priv->data->start_cpu);

	for (i = 0; i < GTOP_LIBGTOP_DATA_TYPE__MAX; i++)
	    g_ptr_array_free (glc->_priv->update_hooks [i], FALSE);

	g_free (glc->_priv->data);
    }

    g_free (glc->_priv);
    glc->_priv = NULL;

    if (GTK_OBJECT_CLASS (parent_class)->finalize)
	(* GTK_OBJECT_CLASS (parent_class)->finalize) (object);
}

void
gtop_libgtop_client_construct (GTopLibGTopClient *glc,
			       GConfClient *gconf_client,
			       glibtop *libgtop_server)
{
    guint i;

    g_return_if_fail (glc != NULL);
    g_return_if_fail (GTOP_IS_LIBGTOP_CLIENT (glc));

    gtk_object_ref (GTK_OBJECT (gconf_client));
    /* glibtop_server_ref (libgtop_server); */

    glc->_priv->client = gconf_client;
    glc->_priv->server = libgtop_server;

    for (i = 0; i < GTOP_LIBGTOP_DATA_TYPE__MAX; i++)
	glc->_priv->update_hooks [i] = g_ptr_array_new ();

    _gtop_libgtop_client_register_private_timers (glc, glc->_priv->data);
}

GtkObject *
gtop_libgtop_client_new (GConfClient *gconf_client,
			 glibtop *libgtop_server)
{
    GTopLibGTopClient *glc;

    g_return_val_if_fail (gconf_client != NULL, NULL);
    g_return_val_if_fail (GCONF_IS_CLIENT (gconf_client), NULL);

    glc = gtk_type_new (gtop_libgtop_client_get_type ());

    gtop_libgtop_client_construct (glc, gconf_client, libgtop_server);

    return GTK_OBJECT (glc);
}

void
gtop_libgtop_client_start (GTopLibGTopClient *glc)
{
    g_return_if_fail (glc != NULL);
    g_return_if_fail (GTOP_IS_LIBGTOP_CLIENT (glc));

    _gtop_libgtop_client_start_all_timers (glc);
}

void
gtop_libgtop_client_stop (GTopLibGTopClient *glc)
{
    g_return_if_fail (glc != NULL);
    g_return_if_fail (GTOP_IS_LIBGTOP_CLIENT (glc));

    _gtop_libgtop_client_stop_all_timers (glc);
}

guint
gtop_libgtop_client_register_update_hook (GTopLibGTopClient *glc,
					  GTopLibGTopDataType hook_type,
					  GTopLibGTopUpdateFunc func,
					  gpointer user_data)
{
    GTopLibGTopUpdateData *update_data;

    g_return_val_if_fail (glc != NULL, 0);
    g_return_val_if_fail (GTOP_IS_LIBGTOP_CLIENT (glc), 0);
    g_return_val_if_fail (hook_type < GTOP_LIBGTOP_DATA_TYPE__MAX, 0);
    g_return_val_if_fail (func != NULL, 0);

    update_data = g_new0 (GTopLibGTopUpdateData, 1);

    update_data->id = ++last_hook_id;
    update_data->func = func;
    update_data->data = glc->_priv->data;
    update_data->user_data = user_data;

    g_ptr_array_add (glc->_priv->update_hooks [hook_type], update_data);

    return update_data->id;
}

void
gtop_libgtop_client_unregister_update_hook (GTopLibGTopClient *glc, guint id)
{
    g_return_if_fail (glc != NULL);
    g_return_if_fail (GTOP_IS_LIBGTOP_CLIENT (glc));
    g_return_if_fail (id > 0);
}


/* =========================================================================
 * private stuff
 * ========================================================================= */

guint
_gtop_libgtop_client_register_timer (GTopLibGTopClient *glc,
				     GTopLibGTopTimerFunc func,
				     GTopLibGTopDataType hook_type,
				     gulong interval, gpointer user_data)
{
    GTopLibGTopTimer *timer;

    g_return_val_if_fail (glc != NULL, 0);
    g_return_val_if_fail (GTOP_IS_LIBGTOP_CLIENT (glc), 0);
    g_return_val_if_fail (hook_type < GTOP_LIBGTOP_DATA_TYPE__MAX, 0);
    g_return_val_if_fail (func != NULL, 0);
    g_return_val_if_fail (interval >= MIN_UPDATE_INTERVAL, 0);

    timer = g_new0 (GTopLibGTopTimer, 1);

    timer->glc = glc;
    timer->hook_type = hook_type;
    timer->id = ++last_timer_id;
    timer->interval = interval;
    timer->func = func;
    timer->data = user_data;

    gtk_object_ref (GTK_OBJECT (timer->glc));

    glc->_priv->timer_list = g_slist_prepend
	(glc->_priv->timer_list, timer);

    return timer->id;
}

void
_gtop_libgtop_client_unregister_timer (GTopLibGTopClient *glc, guint timer_id)
{
    GSList *c;

    g_return_if_fail (glc != NULL);
    g_return_if_fail (GTOP_IS_LIBGTOP_CLIENT (glc));
    g_return_if_fail (timer_id > 0);

    for (c = glc->_priv->timer_list; c; c = c->next) {
	GTopLibGTopTimer *timer = c->data;

	if (timer->id == timer_id) {
	    gtk_object_unref (GTK_OBJECT (timer->glc));

	    if (timer->timer_tag)
		g_source_remove (timer->timer_tag);

	    glc->_priv->timer_list = g_slist_remove
		(glc->_priv->timer_list, timer);

	    g_free (timer);
	    return;
	}
    }
}

static gboolean
timeout_func (gpointer data)
{
    GTopLibGTopTimer *timer = data;
    GPtrArray *hook_array;

    hook_array = timer->glc->_priv->update_hooks [timer->hook_type];

    if (hook_array->len > 0) {
	guint i;

	timer->func (timer->glc, timer->data);

	for (i = 0; i < hook_array->len; i++) {
	    GTopLibGTopUpdateData *data = g_ptr_array_index (hook_array, i);

	    data->func (timer->glc, data->data, data->user_data);
	}
    }

    return TRUE;
}

static void
start_timer (GTopLibGTopTimer *timer, GTopLibGTopClient *glc)
{
    if (timer->timer_tag)
	g_source_remove (timer->timer_tag);

    timer->timer_tag = g_timeout_add (timer->interval, timeout_func, timer);
}

static void
stop_timer (GTopLibGTopTimer *timer, GTopLibGTopClient *glc)
{
    if (timer->timer_tag)
	g_source_remove (timer->timer_tag);
}

void
_gtop_libgtop_client_start_all_timers (GTopLibGTopClient *glc)
{
    g_return_if_fail (glc != NULL);
    g_return_if_fail (GTOP_IS_LIBGTOP_CLIENT (glc));

    g_slist_foreach (glc->_priv->timer_list, (GFunc) start_timer, glc);
}

void
_gtop_libgtop_client_stop_all_timers (GTopLibGTopClient *glc)
{
    g_return_if_fail (glc != NULL);
    g_return_if_fail (GTOP_IS_LIBGTOP_CLIENT (glc));

    g_slist_foreach (glc->_priv->timer_list, (GFunc) stop_timer, glc);
}

/* =========================================================================
 * LibGTop stuff
 * ========================================================================= */

static void
update_cpu (GTopLibGTopClient *glc, GTopLibGTopData *data)
{
    g_message ("update_cpu");

    glibtop_get_cpu_l (glc->_priv->server, &data->cpu);

    if (!data->start_cpu)
	data->start_cpu = g_memdup (&data->cpu, sizeof (glibtop_cpu));
}

static void
update_mem (GTopLibGTopClient *glc, GTopLibGTopData *data)
{
    g_message ("update_mem");
    glibtop_get_mem_l (glc->_priv->server, &data->mem);
}

static void
update_swap (GTopLibGTopClient *glc, GTopLibGTopData *data)
{
    g_message ("update_swap");
    glibtop_get_swap_l (glc->_priv->server, &data->swap);
}

static void
update_uptime (GTopLibGTopClient *glc, GTopLibGTopData *data)
{
    g_message ("update_uptime");
    glibtop_get_uptime_l (glc->_priv->server, &data->uptime);
}

static void
update_loadavg (GTopLibGTopClient *glc, GTopLibGTopData *data)
{
    g_message ("update_loadavg");
    glibtop_get_loadavg_l (glc->_priv->server, &data->loadavg);
}

void
_gtop_libgtop_client_register_private_timers (GTopLibGTopClient *glc,
					      gpointer user_data)
{
    guint id;

    g_return_if_fail (glc != NULL);
    g_return_if_fail (GTOP_IS_LIBGTOP_CLIENT (glc));
    g_return_if_fail (glc->_priv->private_timer_ids == NULL);

    glc->_priv->private_timer_ids = g_array_new (FALSE, TRUE, sizeof (guint));

    id = _gtop_libgtop_client_register_timer
	(glc, (GTopLibGTopTimerFunc) update_cpu,
	 GTOP_LIBGTOP_DATA_TYPE_CPU, 2500, user_data);
    g_array_prepend_val (glc->_priv->private_timer_ids, id);

    id = _gtop_libgtop_client_register_timer
	(glc, (GTopLibGTopTimerFunc) update_mem,
	 GTOP_LIBGTOP_DATA_TYPE_MEM, 2500, user_data);
    g_array_prepend_val (glc->_priv->private_timer_ids, id);

    id = _gtop_libgtop_client_register_timer
	(glc, (GTopLibGTopTimerFunc) update_swap,
	 GTOP_LIBGTOP_DATA_TYPE_SWAP, 2500, user_data);
    g_array_prepend_val (glc->_priv->private_timer_ids, id);

    id = _gtop_libgtop_client_register_timer
	(glc, (GTopLibGTopTimerFunc) update_uptime,
	 GTOP_LIBGTOP_DATA_TYPE_UPTIME, 2500, user_data);
    g_array_prepend_val (glc->_priv->private_timer_ids, id);

    id = _gtop_libgtop_client_register_timer
	(glc, (GTopLibGTopTimerFunc) update_loadavg,
	 GTOP_LIBGTOP_DATA_TYPE_LOADAVG, 2500, user_data);
    g_array_prepend_val (glc->_priv->private_timer_ids, id);
}

void
_gtop_libgtop_client_unregister_private_timers (GTopLibGTopClient *glc)
{
    guint i;

    g_return_if_fail (glc != NULL);
    g_return_if_fail (GTOP_IS_LIBGTOP_CLIENT (glc));

    if (glc->_priv->private_timer_ids == NULL)
	return;

    for (i = 0; i < glc->_priv->private_timer_ids->len; i++) {
	guint id = g_array_index (glc->_priv->private_timer_ids, guint, i);

	_gtop_libgtop_client_unregister_timer (glc, id);
    }

    g_array_free (glc->_priv->private_timer_ids, TRUE);
    glc->_priv->private_timer_ids = NULL;
}

