/* -*- Mode: C; c-set-style: gnu indent-tabs-mode: t; c-basic-offset: 4; tab-width: 8 -*- */
/*
 * Copyright (C) 2000 SuSE GmbH
 * Author: Martin Baulig <baulig@suse.de>
 *
 * This file is part of GTop.
 *
 * GTop is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * GTop is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with GTop; see the file COPYING.  If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GTOP_LIBGTOP_CLIENT_H__
#define __GTOP_LIBGTOP_CLIENT_H__

#include <config.h>
#include <gnome.h>
#include <gconf/gconf-client.h>

#include <glibtop.h>
#include <glibtop/union.h>
#include <glibtop/close.h>

BEGIN_GNOME_DECLS

#define GTOP_TYPE_LIBGTOP_CLIENT            (gtop_libgtop_client_get_type ())
#define GTOP_LIBGTOP_CLIENT(obj)            (GTK_CHECK_CAST ((obj), GTOP_TYPE_LIBGTOP_CLIENT, GTopLibGTopClient))
#define GTOP_LIBGTOP_CLIENT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GTOP_TYPE_LIBGTOP_CLIENT, GTopLibGTopClientClass))
#define GTOP_IS_LIBGTOP_CLIENT(obj)         (GTK_CHECK_TYPE ((obj), GTOP_TYPE_LIBGTOP_CLIENT))
#define GTOP_IS_LIBGTOP_CLIENT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GTOP_TYPE_LIBGTOP_CLIENT))
#define GTOP_LIBGTOP_CLIENT_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), GTOP_TYPE_LIBGTOP_CLIENT, GTopLibGTopClientClass))

typedef struct _GTopLibGTopClient           GTopLibGTopClient;
typedef struct _GTopLibGTopClientPrivate    GTopLibGTopClientPrivate;
typedef struct _GTopLibGTopClientClass      GTopLibGTopClientClass;

typedef struct _GTopLibGTopUpdateData       GTopLibGTopUpdateData;

typedef enum   _GTopLibGTopDataType         GTopLibGTopDataType;
typedef struct _GTopLibGTopData             GTopLibGTopData;

typedef void (*GTopLibGTopUpdateFunc)       (GTopLibGTopClient *,
					     GTopLibGTopData *,
					     gpointer);

enum _GTopLibGTopDataType {
    GTOP_LIBGTOP_DATA_TYPE_CPU = 0,
    GTOP_LIBGTOP_DATA_TYPE_MEM,
    GTOP_LIBGTOP_DATA_TYPE_SWAP,
    GTOP_LIBGTOP_DATA_TYPE_UPTIME,
    GTOP_LIBGTOP_DATA_TYPE_LOADAVG,
    GTOP_LIBGTOP_DATA_TYPE__MAX
};

struct _GTopLibGTopData {
    /* CPU Usage at application start or after last reset. */
    const glibtop_cpu  *start_cpu;

    glibtop_cpu         cpu;
    glibtop_mem         mem;
    glibtop_swap        swap;
    glibtop_uptime      uptime;
    glibtop_loadavg     loadavg;
};

struct _GTopLibGTopClient {
    GtkObject object;

    /*< private >*/
    GTopLibGTopClientPrivate *_priv;
};

struct _GTopLibGTopClientClass {
    GtkObjectClass parent_class;
};

guint
gtop_libgtop_client_get_type                   (void);

GtkObject *
gtop_libgtop_client_new                        (GConfClient *gconf_client,
						glibtop *libgtop_server);

void
gtop_libgtop_client_construct                  (GTopLibGTopClient *glc,
						GConfClient *gconf_client,
						glibtop *libgtop_server);

void
gtop_libgtop_client_start                      (GTopLibGTopClient *glc);

void
gtop_libgtop_client_stop                       (GTopLibGTopClient *glc);

guint
gtop_libgtop_client_register_update_hook       (GTopLibGTopClient *glc,
                                                GTopLibGTopDataType hook_type,
                                                GTopLibGTopUpdateFunc func,
                                                gpointer user_data);

void
gtop_libgtop_client_unregister_update_hook     (GTopLibGTopClient *glc,
                                                guint id);

END_GNOME_DECLS

#endif /* __GTOP_LIBGTOP_CLIENT_H__ */

