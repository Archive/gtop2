
#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#include <config.h>
#include <gnome.h>
#include <gconf/gconf-client.h>

#ifdef HAVE_LIMITS_H
#include <limits.h>
#endif

#include <glib.h>

BEGIN_GNOME_DECLS

/* stolen from glib 1.3.x */
#ifndef G_STRINGIFY
#define G_STRINGIFY(macro_or_string)    G_STRINGIFY_ARG (macro_or_string)
#define G_STRINGIFY_ARG(contents)       #contents
#endif

/* stolen from glib 1.3.x */
#ifndef G_STRLOC
/* provide a string identifying the current code position */
#ifdef  __GNUC__
#  define G_STRLOC      __FILE__ ":" G_STRINGIFY (__LINE__) ":" __PRETTY_FUNCTION__ "()"
#else
#  define G_STRLOC      __FILE__ ":" G_STRINGIFY (__LINE__)
#endif
#endif

GtkWidget *
global_properties_init (GConfClient *client);

GtkWidget *
summary_properties_init (GConfClient *client);

GtkWidget *
summary_colors_properties_init (GConfClient *client);

END_GNOME_DECLS

#endif /* __GLOBAL_H__ */

