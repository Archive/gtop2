#include <global.h>
#include <properties.h>

#include <gnome.h>
#include <libgnomeui/gnome-window-icon.h>

#include <locale.h>

static GnomeUIInfo file_menu [] = {
    GNOMEUIINFO_MENU_EXIT_ITEM (gtk_main_quit, NULL),
    GNOMEUIINFO_END
};

static GnomeUIInfo main_menu [] = {
    GNOMEUIINFO_MENU_FILE_TREE (file_menu),
    GNOMEUIINFO_END
};

int
main (int argc, char *argv[])
{
    GtkWidget *app, *nb, *global, *summary, *summary_colors;
    GConfClient *client;

    setlocale (LC_ALL, "");
    bindtextdomain (PACKAGE, GNOMELOCALEDIR);
    textdomain (PACKAGE);

    gnome_init ("gtop", VERSION, argc, argv);
    gnome_window_icon_set_default_from_file (GNOME_ICONDIR"/gnome-gtop.png");

    gconf_init (argc, argv, NULL);
    client = gconf_client_get_default ();

    gconf_client_add_dir (client, GTOP_GCONF_DIR, GCONF_CLIENT_PRELOAD_NONE,
			  NULL);

    /* The main() function takes over the floating object; the code that
       "owns" the object should do this, as with any Gtk object.
       Read about refcounting and destruction at developer.gnome.org/doc/GGAD/ */
    gtk_object_ref (GTK_OBJECT (client));
    gtk_object_sink (GTK_OBJECT (client));

    app = gnome_app_new ("gtop", "GTop");

    gnome_app_create_menus (GNOME_APP (app), main_menu);

    nb = gtk_notebook_new ();

    global = global_properties_init (client);

    gtk_notebook_append_page (GTK_NOTEBOOK (nb), global,
			      gtk_label_new (_("Global")));

    summary = summary_properties_init (client);

    gtk_notebook_append_page (GTK_NOTEBOOK (nb), summary,
			      gtk_label_new (_("Summary")));
 
    summary_colors = summary_colors_properties_init (client);

    gtk_notebook_append_page (GTK_NOTEBOOK (nb), summary_colors,
			      gtk_label_new (_("Summary Colors")));
 
    gnome_app_set_contents (GNOME_APP (app), nb);

    gtk_widget_show_all (app);

    /* enter gtk main */
    gtk_main ();

    /* Shut down the client cleanly. Note the destroy rather than unref,
       so the shutdown occurs even if there are outstanding references.
       If your program isn't exiting you probably want to just plain unref()
       Read about refcounting and destruction at developer.gnome.org/doc/GGAD/ */
    gtk_object_destroy (GTK_OBJECT (client));
    /* Now avoid leaking memory (not that this matters since the program
       is exiting... */
    gtk_object_unref (GTK_OBJECT (client));

    g_mem_profile ();

    return 0;
}
