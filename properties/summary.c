/* -*- Mode: C; c-set-style: gnu indent-tabs-mode: t; c-basic-offset: 4; tab-width: 8 -*- */
/*
 * Copyright (C) 2000 SuSE GmbH
 * Author: Martin Baulig <baulig@suse.de>
 *
 * This file is part of GTop.
 *
 * GTop is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * GTop is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with GTop; see the file COPYING.  If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <global.h>
#include <properties.h>

GtkWidget *
summary_properties_init (GConfClient *client)
{
    GtkWidget *vb, *hb1, *frame, *label, *table, *spin, *hb;
    GtkObject *adjustment;

    vb = gtk_vbox_new (FALSE, 0);
    gtk_container_border_width (GTK_CONTAINER (vb), GNOME_PAD_SMALL);

    frame = gtk_frame_new (_("Statusbar Font"));

    table = gtop_properties_font_entry_new (client, _("Statusbar Font"),
					    "/summary/statusbar_font");

    gtk_container_add (GTK_CONTAINER (frame), table);
    gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);
    gtk_box_pack_start (GTK_BOX (vb), frame, FALSE, TRUE, 0);

    frame = gtk_frame_new (_("Graphical Summary"));
    table = gtk_table_new (7, 3, FALSE);
    gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD << 1);
    gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);

    label = gtk_check_button_new_with_label (_("Show graphical summary"));
    gtop_properties_check_button_new (client, "/summary/show_graphical",
				      label);

#if 0
    if (!(summary_supported & (1 << GTOP_SUMMARY_SHOW_GRAPH)))
	gtk_widget_set_sensitive (label, FALSE);
#endif
	
    gtk_table_attach (GTK_TABLE (table), label, 0, 3, 0, 1,
		      GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);

    label = gtk_check_button_new_with_label (_("Show cpu usage"));
    gtop_properties_check_button_new (client, "/summary/show_cpu",
				      label);

#if 0
    if (!(summary_supported & (1 << GTOP_SUMMARY_GRAPH_CPU)))
	gtk_widget_set_sensitive (label, FALSE);
#endif
	
    gtk_table_attach (GTK_TABLE (table), label, 1, 3, 1, 2,
		      GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);

    label = gtk_check_button_new_with_label (_("Enable SMP support"));
    gtop_properties_check_button_new (client, "/summary/enable_cpu_smp",
				      label);

#if 0	
    if (!(summary_supported & (1 << GTOP_SUMMARY_GRAPH_XCPU)))
	gtk_widget_set_sensitive (label, FALSE);
#endif
	
    gtk_table_attach (GTK_TABLE (table), label, 2, 3, 2, 3,
		      GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);

    label = gtk_check_button_new_with_label (_("Show physical memory usage"));
    gtop_properties_check_button_new (client, "/summary/show_memusage",
				      label);

#if 0
    if (!(summary_supported & (1 << GTOP_SUMMARY_GRAPH_MEM)))
	gtk_widget_set_sensitive (label, FALSE);
#endif
	
    gtk_table_attach (GTK_TABLE (table), label, 1, 3, 3, 4,
		      GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);

    label = gtk_check_button_new_with_label (_("Show swap memory usage"));
    gtop_properties_check_button_new (client, "/summary/show_swapusage",
				      label);

#if 0	
    if (!(summary_supported & (1 << GTOP_SUMMARY_GRAPH_SWAP)))
	gtk_widget_set_sensitive (label, FALSE);
#endif
	
    gtk_table_attach (GTK_TABLE (table), label, 1, 3, 4, 5,
		      GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);

    label = gtk_check_button_new_with_label (_("Show load average"));
    gtop_properties_check_button_new (client, "/summary/show_loadavg",
				      label);

#if 0	
    if (!(summary_supported & (1 << GTOP_SUMMARY_GRAPH_LOAD)))
	gtk_widget_set_sensitive (label, FALSE);
#endif
	
    gtk_table_attach (GTK_TABLE (table), label, 1, 3, 5, 6,
		      GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);

    gtk_container_add (GTK_CONTAINER (frame), table);
    hb1 = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
    gtk_box_pack_start (GTK_BOX (hb1), frame, TRUE, TRUE, 0);

    hb = gtk_hbox_new (FALSE, GNOME_PAD);

    label = gtk_label_new (_("Maximum load average:"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_box_pack_start_defaults (GTK_BOX (hb), label);

#if 0
    if (!(summary_supported & (1 << GTOP_SUMMARY_GRAPH_LOAD)))
	gtk_widget_set_sensitive (label, FALSE);
#endif
	
    spin = gtk_spin_button_new (NULL, 0.1, 1);
    adjustment = gtk_adjustment_new (1.0, 1.0, 10000.0, 0.1, 1.0, 1.0);
    gtk_spin_button_set_adjustment
	(GTK_SPIN_BUTTON (spin), GTK_ADJUSTMENT (adjustment));
    gtop_properties_adjustment_new (client, "/summary/max_loadavg",
				    GTK_ADJUSTMENT (adjustment));
    gtk_widget_set_usize (spin, 100, -1);
    gtk_box_pack_start_defaults (GTK_BOX (hb), spin);
    gtk_table_attach (GTK_TABLE (table), hb, 2, 3, 6, 7,
		      GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);

#if 0
    if (!(summary_supported & (1 << GTOP_SUMMARY_GRAPH_LOAD)))
	gtk_widget_set_sensitive (spin, FALSE);
#endif
	
    frame = gtk_frame_new (_("Textual Summary"));
    table = gtk_table_new (9, 3, FALSE);
    gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD << 1);
    gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);

    label = gtk_check_button_new_with_label (_("Show text summary"));
    gtop_properties_check_button_new (client, "/summary/show_textual",
				      label);

    gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 3, 0, 1);

    label = gtk_check_button_new_with_label (_("Show statusbar"));
    gtop_properties_check_button_new (client, "/summary/show_statusbar",
				      label);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 1, 3, 1, 2);

    label = gtk_check_button_new_with_label (_("include hostname"));
    gtop_properties_check_button_new (client, "/summary/show_hostname",
				      label);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, 2, 3);

    label = gtk_check_button_new_with_label (_("use fully qualified name"));
    gtop_properties_check_button_new (client, "/summary/use_fqdn",
				      label);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, 3, 4);

    label = gtk_check_button_new_with_label (_("Show cpu statistics"));
    gtop_properties_check_button_new (client, "/summary/show_text_cpu",
				      label);

#if 0
    if (!(summary_supported & (1 << GTOP_SUMMARY_TEXT_CPU)))
	gtk_widget_set_sensitive (label, FALSE);
#endif

    gtk_table_attach_defaults (GTK_TABLE (table), label, 1, 3, 4, 5);

    label = gtk_check_button_new_with_label (_("Show memory statistics"));
    gtop_properties_check_button_new (client, "/summary/show_text_mem",
				      label);

#if 0	
    if (!(summary_supported & (1 << GTOP_SUMMARY_TEXT_MEMORY)))
	gtk_widget_set_sensitive (label, FALSE);
#endif

    gtk_table_attach_defaults (GTK_TABLE (table), label, 1, 3, 5, 6);

    label = gtk_check_button_new_with_label (_("include swap statistics"));
    gtop_properties_check_button_new (client, "/summary/show_text_swap",
				      label);

#if 0	
    if (!(summary_supported & (1 << GTOP_SUMMARY_TEXT_SWAP)))
	gtk_widget_set_sensitive (label, FALSE);
#endif

    gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, 6, 7);

    label = gtk_check_button_new_with_label (_("Show uptime"));
    gtop_properties_check_button_new (client, "/summary/show_text_uptime",
				      label);

#if 0	
    if (!(summary_supported & (1 << GTOP_SUMMARY_TEXT_UPTIME)))
	gtk_widget_set_sensitive (label, FALSE);
#endif

    gtk_table_attach_defaults (GTK_TABLE (table), label, 1, 3, 7, 8);

    label = gtk_check_button_new_with_label (_("Show load average"));
    gtop_properties_check_button_new (client, "/summary/show_text_loadavg",
				      label);

#if 0	
    if (!(summary_supported & (1 << GTOP_SUMMARY_TEXT_LOADAVG)))
	gtk_widget_set_sensitive (label, FALSE);
#endif

    gtk_table_attach_defaults (GTK_TABLE (table), label, 1, 3, 8, 9);

    gtk_container_add (GTK_CONTAINER (frame), table);
    gtk_box_pack_start (GTK_BOX (hb1), frame, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vb), hb1, FALSE, TRUE, GNOME_PAD_SMALL);

    return vb;
}

static const gchar *cpu_texts [4] = {
	N_("User"),  N_("Nice"),   N_("System"),  N_("Idle")
};

static const gchar *mem_texts [4] =  {
	N_("Other"), N_("Shared"), N_("Buffers"), N_("Free")
};

static const gchar *swap_texts [2] = {
	N_("Used"), N_("Free")
};

static const gchar *load_texts [2] = {
	N_("Used"), N_("Free")
};

GtkWidget *
summary_colors_properties_init (GConfClient *client)
{
    GtkWidget *vb, *frame, *table;
    gint i;
	
    vb = gtk_vbox_new (FALSE, 0);
    gtk_container_border_width (GTK_CONTAINER (vb), GNOME_PAD_SMALL);

    frame = gtk_frame_new (_("Summary CPU"));

    table = gtk_table_new (2, 4, TRUE);
    gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
    gtk_container_set_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);

    for (i = 0; i < 4; i++) {
	GtkWidget *label, *cp;
	gchar *config_key;

	label = gtk_label_new (_(cpu_texts [i]));
	gtk_table_attach (GTK_TABLE (table), label, i, i+1, 1, 2,
			  GTK_EXPAND, GTK_FILL, 0, 0);

	cp = gnome_color_picker_new ();

	config_key = g_strdup_printf ("gtop/summary_cpu/color%d", i);
	gtop_properties_color_entry_new (client, config_key,
					 GNOME_COLOR_PICKER (cp));
	g_free (config_key);

	gtk_table_attach (GTK_TABLE (table), cp, i, i+1, 0, 1,
			  GTK_FILL, GTK_SHRINK, 0, 0);
    }

    gtk_container_add (GTK_CONTAINER (frame), table);

    gtk_box_pack_start (GTK_BOX (vb), frame, FALSE, TRUE, 0);

    frame = gtk_frame_new (_("Summary physical memory"));

    table = gtk_table_new (2, 4, TRUE);
    gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
    gtk_container_set_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);

    for (i = 0; i < 4; i++) {
	GtkWidget *label, *cp;
	gchar *config_key;

	label = gtk_label_new (_(mem_texts [i]));
	gtk_table_attach (GTK_TABLE (table), label, i, i+1, 1, 2,
			  GTK_EXPAND, GTK_FILL, 0, 0);

	cp = gnome_color_picker_new ();

	config_key = g_strdup_printf ("gtop/summary_mem/color%d", i);
	gtop_properties_color_entry_new (client, config_key,
					 GNOME_COLOR_PICKER (cp));
	g_free (config_key);

	gtk_table_attach (GTK_TABLE (table), cp, i, i+1, 0, 1,
			  GTK_FILL, GTK_SHRINK, 0, 0);
    }

    gtk_container_add (GTK_CONTAINER (frame), table);

    gtk_box_pack_start (GTK_BOX (vb), frame, FALSE, TRUE, 0);

    frame = gtk_frame_new (_("Summary swap memory"));

    table = gtk_table_new (2, 4, TRUE);
    gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
    gtk_container_set_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);

    for (i = 0; i < 2; i++) {
	GtkWidget *label, *cp;
	gchar *config_key;
	gint col;

	col = i ? 0 : 3;

	label = gtk_label_new (_(swap_texts [i]));
	gtk_table_attach (GTK_TABLE (table), label, col, col+1, 1, 2,
			  GTK_EXPAND, GTK_FILL, 0, 0);

	cp = gnome_color_picker_new ();

	config_key = g_strdup_printf ("gtop/summary_swap/color%d", i);
	gtop_properties_color_entry_new (client, config_key,
					 GNOME_COLOR_PICKER (cp));
	g_free (config_key);

	gtk_table_attach (GTK_TABLE (table), cp, col, col+1, 0, 1,
			  GTK_FILL, GTK_SHRINK, 0, 0);
    }

    gtk_container_add (GTK_CONTAINER (frame), table);

    gtk_box_pack_start (GTK_BOX (vb), frame, FALSE, TRUE, 0);

    frame = gtk_frame_new (_("Summary load average"));

    table = gtk_table_new (2, 4, TRUE);
    gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
    gtk_container_set_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);

    for (i = 0; i < 2; i++) {
	GtkWidget *label, *cp;
	gchar *config_key;
	gint col;

	col = i ? 0 : 3;

	label = gtk_label_new (_(load_texts [i]));
	gtk_table_attach (GTK_TABLE (table), label, col, col+1, 1, 2,
			  GTK_EXPAND, GTK_FILL, 0, 0);

	cp = gnome_color_picker_new ();

	config_key = g_strdup_printf ("gtop/summary_load/color%d", i);
	gtop_properties_color_entry_new (client, config_key,
					 GNOME_COLOR_PICKER (cp));
	g_free (config_key);

	gtk_table_attach (GTK_TABLE (table), cp, col, col+1, 0, 1,
			  GTK_FILL, GTK_SHRINK, 0, 0);
    }

    gtk_container_add (GTK_CONTAINER (frame), table);

    gtk_box_pack_start (GTK_BOX (vb), frame, FALSE, TRUE, 0);

    return vb;
}

