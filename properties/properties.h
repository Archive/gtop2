/* -*- Mode: C; c-set-style: gnu indent-tabs-mode: t; c-basic-offset: 4; tab-width: 8 -*- */
/*
 * Copyright (C) 2000 SuSE GmbH
 * Author: Martin Baulig <baulig@suse.de>
 *
 * This file is part of GTop.
 *
 * GTop is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * GTop is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with GTop; see the file COPYING.  If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __PROPERTIES_H__
#define __PROPERTIES_H__

#include <global.h>
#include <gconf/gconf-client.h>

BEGIN_GNOME_DECLS

#define GTOP_GCONF_DIR		"/apps/gtop"

typedef struct	_GTopProperties			GTopProperties;

typedef struct	_GTopGlobalProperties		GTopGlobalProperties;
typedef struct	_GTopGraphProperties		GTopGraphProperties;
typedef struct	_GTopMemUsageProperties		GTopMemUsageProperties;
typedef struct	_GTopFsUsageProperties		GTopFsUsageProperties;
typedef struct	_GTopProcViewProperties	       	GTopProcViewProperties;
typedef struct	_GTopProcFieldsProperties	GTopProcFieldsProperties;
typedef struct	_GTopSummaryProperties		GTopSummaryProperties;
typedef struct	_GTopSummaryColorsProperties	GTopSummaryColorsProperties;

typedef enum	_GTopProcSelect			GTopProcSelect;
typedef enum	_GTopPropFsMode			GTopPropFsMode;
typedef enum	_GTopSummaryMode		GTopSummaryMode;
typedef enum	_GTopUpdateTimes		GTopUpdateTimes;
typedef enum	_GTopDetailsFlags		GTopDetailsFlags;

#define GTOP_PROCFIELD_COUNT	15

#define UPDATE_FIELDS		11
#define MEMUSAGE_FIELDS		5
#define GRAPH_DEFAULT_COLORS	4

enum _GTopPropFsMode {
    GTOP_FSMODE_SUBLOCKS = 0,
    GTOP_FSMODE_BLOCKS,
    GTOP_FSMODE_INODES
};

enum _GTopProcSelect {
    GTOP_PROC_SELECT_ALL = 0,
    GTOP_PROC_SELECT_USER,
    GTOP_PROC_SELECT_TTY
};

enum _GTopSummaryMode {
    GTOP_SUMMARY_SHOW_TEXT = 0,
    GTOP_SUMMARY_TEXT_STATUSBAR,
    GTOP_SUMMARY_TEXT_HOSTNAME,
    GTOP_SUMMARY_TEXT_USE_FQDN,
    GTOP_SUMMARY_TEXT_CPU,
    GTOP_SUMMARY_TEXT_MEMORY,
    GTOP_SUMMARY_TEXT_SWAP,
    GTOP_SUMMARY_TEXT_UPTIME,
    GTOP_SUMMARY_TEXT_LOADAVG,
    GTOP_SUMMARY_SHOW_GRAPH,
    GTOP_SUMMARY_GRAPH_CPU,
    GTOP_SUMMARY_GRAPH_XCPU,
    GTOP_SUMMARY_GRAPH_MEM,
    GTOP_SUMMARY_GRAPH_SWAP,
    GTOP_SUMMARY_GRAPH_LOAD,
};

enum _GTopUpdateTimes {
    GTOP_UPDATE_CPU = 0,
    GTOP_UPDATE_MEM,
    GTOP_UPDATE_PROCVIEW,
    GTOP_UPDATE_MEMUSAGE,
    GTOP_UPDATE_FSUSAGE,
    GTOP_UPDATE_LOAD,
    GTOP_UPDATE_STATUS_CPU,
    GTOP_UPDATE_STATUS_MEMORY,
    GTOP_UPDATE_STATUS_UPTIME,
    GTOP_UPDATE_STATUS_LOADAVG,
    GTOP_UPDATE_DETAILS
};

enum _GTopDetailsFlags {
    GTOP_DETAILS_AUTO_UPDATE = 1,
    GTOP_DETAILS_REMEMBER_POSITION,
    GTOP_DETAILS_CUMULATIVE_TIMINGS,
    GTOP_DETAILS_FULL_PATHNAMES
};

struct _GTopGlobalProperties
{
    gint save_session;
    GnomeMDIMode mdi_mode;
    glong update_times [UPDATE_FIELDS];

    gint show_menubar;
    gint show_toolbar;
};

struct _GTopMemUsageProperties
{
    GTopProcSelect proc_select;
    glong thresholds [MEMUSAGE_FIELDS];
};

struct _GTopFsUsageProperties
{
    GTopPropFsMode fsmode;
    gint selected_fs;
    glong selected_fs_mask;
};

struct _GTopProcViewProperties
{
    GdkFont *font;
    gchar *font_name;
    glong details_flags;
};

struct _GTopSummaryProperties
{
    GdkFont *statusbar_font;
    gchar *statusbar_font_name;
    glong summary_mode;
    glong summary_supported;
    gfloat maximum_loadavg;
};

struct _GTopProcFieldsProperties
{
    glong field_mask;
    gint field_width [GTOP_PROCFIELD_COUNT];
};

struct _GTopSummaryColorsProperties
{
    GdkColor cpu [4];
    GdkColor mem [4];
    GdkColor swap [2];
    GdkColor load [2];
};

struct _GTopGraphProperties
{
    GdkFont *font;
    gchar *font_name;
    GdkColor colors [GRAPH_DEFAULT_COLORS+2];
    guint default_width;
    guint default_height;
    guint horizontal_border;
    guint vertical_border;
    guint graph_width;
    guint extra_height;
    guint line_width;
    guint pad_width;
};

struct _GTopProperties {
    GTopGlobalProperties global;
    GTopGraphProperties graph;
    GTopMemUsageProperties memusage;
    GTopFsUsageProperties fsusage;
    GTopProcViewProperties procview;
    GTopProcFieldsProperties procfields;
    GTopSummaryProperties summary;
    GTopSummaryColorsProperties summary_colors;
};

void
gtop_properties_check_button_new (GConfClient *client,
				  const gchar *config_key,
				  GtkWidget *button);

void
gtop_properties_check_button_set (GtkCheckButton *button,
				  GConfValue *value);

GConfValue *
gtop_properties_check_button_get (GtkCheckButton *button,
				  GConfValueType type);

void
gtop_properties_adjustment_new (GConfClient *client,
				const gchar *config_key,
				GtkAdjustment *adjustment);

GConfValue *
gtop_properties_adjustment_get (GtkAdjustment *adjustment,
				GConfValueType type);

void
gtop_properties_adjustment_set (GtkAdjustment *adjustment,
				GConfValue *value);

void
gtop_properties_radio_button_new (GConfClient *client,
				  const gchar *config_key,
				  GtkRadioButton *button);

void
gtop_properties_radio_button_set (GtkRadioButton *button,
				  GConfValue *value);

GConfValue *
gtop_properties_radio_button_get (GtkRadioButton *button,
				  GConfValueType type);

GtkWidget *
gtop_properties_font_entry_new (GConfClient *client,
				const gchar *label,
				const gchar *config_key);

void
gtop_properties_color_entry_new (GConfClient *client,
				 const gchar *config_key,
				 GnomeColorPicker *picker);

void
gtop_properties_color_entry_set (GnomeColorPicker *picker,
				 GConfValue *value);

GConfValue *
gtop_properties_color_entry_get (GnomeColorPicker *picker,
				 GConfValueType type);

END_GNOME_DECLS

#endif /* __PROPERTIES_H__ */
