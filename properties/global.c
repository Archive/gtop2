/* -*- Mode: C; c-set-style: gnu indent-tabs-mode: t; c-basic-offset: 4; tab-width: 8 -*- */
/*
 * Copyright (C) 2000 SuSE GmbH
 * Author: Martin Baulig <baulig@suse.de>
 *
 * This file is part of GTop.
 *
 * GTop is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * GTop is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with GTop; see the file COPYING.  If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <gnome.h>
#include <global.h>

#include <properties.h>

GtkWidget *
global_properties_init (GConfClient *client)
{
    GtkWidget *vb, *hb, *frame, *button, *table, *label, *spin;
    GtkObject *adjustment;
    GSList *group;

    vb = gtk_vbox_new (0, FALSE);
    gtk_container_border_width (GTK_CONTAINER (vb), GNOME_PAD_SMALL);

    frame = gtk_frame_new (_("Session management"));
    table = gtk_table_new (1, 1, FALSE);
    gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD);

    button = gtk_check_button_new_with_label (_("Always save session"));
    gtop_properties_check_button_new (client, "/global/always_save_session",
				      button);

    gtk_table_attach_defaults (GTK_TABLE (table), button, 0, 1, 0, 1);

    gtk_container_add (GTK_CONTAINER (frame), table);
    gtk_box_pack_start (GTK_BOX (vb), frame, FALSE, TRUE, 0);

    frame = gtk_frame_new (_("Update times"));
    table = gtk_table_new (3, 3, FALSE);
    gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD);
    gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
    gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD);

    label = gtk_label_new (_("Process List"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);

    spin = gtk_spin_button_new (NULL, 1, 0);
    adjustment = gtk_adjustment_new (1, 1, INT_MAX, 1, 100, 100);
    gtk_spin_button_set_adjustment
	(GTK_SPIN_BUTTON (spin), GTK_ADJUSTMENT (adjustment));
    gtop_properties_adjustment_new (client, "/global/update_procview",
				    GTK_ADJUSTMENT (adjustment));
    gtk_table_attach_defaults (GTK_TABLE (table), spin, 1, 2, 0, 1);

    label = gtk_label_new (_("ms"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, 0, 1);

    label = gtk_label_new (_("Memory Usage"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);

    spin = gtk_spin_button_new (NULL, 1, 0);
    adjustment = gtk_adjustment_new (1, 1, INT_MAX, 1, 100, 100);
    gtk_spin_button_set_adjustment
	(GTK_SPIN_BUTTON (spin), GTK_ADJUSTMENT (adjustment));
    gtop_properties_adjustment_new (client, "/global/update_memusage",
				    GTK_ADJUSTMENT (adjustment));
    gtk_table_attach_defaults (GTK_TABLE (table), spin, 1, 2, 1, 2);

    label = gtk_label_new (_("ms"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, 1, 2);

    label = gtk_label_new (_("Filesystem Usage"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 2, 3);

    spin = gtk_spin_button_new (NULL, 1, 0);
    adjustment = gtk_adjustment_new (1, 1, INT_MAX, 1, 100, 100);
    gtk_spin_button_set_adjustment
	(GTK_SPIN_BUTTON (spin), GTK_ADJUSTMENT (adjustment));
    gtop_properties_adjustment_new (client, "/global/update_fsusage",
				    GTK_ADJUSTMENT (adjustment));
    gtk_table_attach_defaults (GTK_TABLE (table), spin, 1, 2, 2, 3);

    label = gtk_label_new (_("ms"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, 2, 3);

    gtk_container_add (GTK_CONTAINER (frame), table);
    hb = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
    gtk_box_pack_start (GTK_BOX (hb), frame, TRUE, TRUE, 0);

    frame = gtk_frame_new (_("Update times (graphical summary)"));
    table = gtk_table_new (3, 3, FALSE);
    gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD);
    gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
    gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD);

    label = gtk_label_new (_("Summary CPU"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);

    spin = gtk_spin_button_new (NULL, 1, 0);
    adjustment = gtk_adjustment_new (1, 1, INT_MAX, 1, 100, 100);
    gtk_spin_button_set_adjustment
	(GTK_SPIN_BUTTON (spin), GTK_ADJUSTMENT (adjustment));
    gtop_properties_adjustment_new (client, "/global/update_cpu",
				    GTK_ADJUSTMENT (adjustment));
    gtk_table_attach_defaults (GTK_TABLE (table), spin, 1, 2, 0, 1);

    label = gtk_label_new (_("ms"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, 0, 1);

    label = gtk_label_new (_("Summary Mem and Swap"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);

    spin = gtk_spin_button_new (NULL, 1, 0);
    adjustment = gtk_adjustment_new (1, 1, INT_MAX, 1, 100, 100);
    gtk_spin_button_set_adjustment
	(GTK_SPIN_BUTTON (spin), GTK_ADJUSTMENT (adjustment));
    gtop_properties_adjustment_new (client, "/global/update_mem",
				    GTK_ADJUSTMENT (adjustment));
    gtk_table_attach_defaults (GTK_TABLE (table), spin, 1, 2, 1, 2);

    label = gtk_label_new (_("ms"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, 1, 2);

    label = gtk_label_new (_("Load Average"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 2, 3);

    spin = gtk_spin_button_new (NULL, 1, 0);
    adjustment = gtk_adjustment_new (1, 1, INT_MAX, 1, 100, 100);
    gtk_spin_button_set_adjustment
	(GTK_SPIN_BUTTON (spin), GTK_ADJUSTMENT (adjustment));
    gtop_properties_adjustment_new (client, "/global/update_loadavg",
				    GTK_ADJUSTMENT (adjustment));
    gtk_table_attach_defaults (GTK_TABLE (table), spin, 1, 2, 2, 3);

    label = gtk_label_new (_("ms"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, 2, 3);

    gtk_container_add (GTK_CONTAINER (frame), table);
    gtk_box_pack_start (GTK_BOX (hb), frame, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vb), hb, FALSE, TRUE, 0);

    frame = gtk_frame_new (_("Update times (textual summary)"));
    table = gtk_table_new (5, 5, FALSE);
    gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD);
    gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
    gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD);

    label = gtk_label_new (_("CPU statistics"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);

    spin = gtk_spin_button_new (NULL, 1, 0);
    adjustment = gtk_adjustment_new (1, 1, INT_MAX, 1, 100, 100);
    gtk_spin_button_set_adjustment
	(GTK_SPIN_BUTTON (spin), GTK_ADJUSTMENT (adjustment));
    gtop_properties_adjustment_new (client, "/global/update_status_cpu",
				    GTK_ADJUSTMENT (adjustment));
    gtk_table_attach_defaults (GTK_TABLE (table), spin, 1, 2, 0, 1);

    label = gtk_label_new (_("ms"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, 0, 1);

    label = gtk_label_new (_("Memory statistics"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);

    spin = gtk_spin_button_new (NULL, 1, 0);
    adjustment = gtk_adjustment_new (1, 1, INT_MAX, 1, 100, 100);
    gtk_spin_button_set_adjustment
	(GTK_SPIN_BUTTON (spin), GTK_ADJUSTMENT (adjustment));
    gtop_properties_adjustment_new (client, "/global/update_status_mem",
				    GTK_ADJUSTMENT (adjustment));
    gtk_table_attach_defaults (GTK_TABLE (table), spin, 1, 2, 1, 2);

    label = gtk_label_new (_("ms"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, 1, 2);

    label = gtk_label_new (_("Uptime"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 2, 3);

    spin = gtk_spin_button_new (NULL, 1, 0);
    adjustment = gtk_adjustment_new (1, 1, INT_MAX, 1, 100, 100);
    gtk_spin_button_set_adjustment
	(GTK_SPIN_BUTTON (spin), GTK_ADJUSTMENT (adjustment));
    gtop_properties_adjustment_new (client, "/global/update_status_uptime",
				    GTK_ADJUSTMENT (adjustment));
    gtk_table_attach_defaults (GTK_TABLE (table), spin, 1, 2, 2, 3);

    label = gtk_label_new (_("ms"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, 2, 3);

    label = gtk_label_new (_("Load average"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 3, 4);

    spin = gtk_spin_button_new (NULL, 1, 0);
    adjustment = gtk_adjustment_new (1, 1, INT_MAX, 1, 100, 100);
    gtk_spin_button_set_adjustment
	(GTK_SPIN_BUTTON (spin), GTK_ADJUSTMENT (adjustment));
    gtop_properties_adjustment_new (client, "/global/update_status_loadavg",
				    GTK_ADJUSTMENT (adjustment));
    gtk_table_attach_defaults (GTK_TABLE (table), spin, 1, 2, 3, 4);

    label = gtk_label_new (_("ms"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, 3, 4);

    label = gtk_label_new (_("Details dialog"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 4, 5);

    spin = gtk_spin_button_new (NULL, 1, 0);
    adjustment = gtk_adjustment_new (1, 1, INT_MAX, 1, 100, 100);
    gtk_spin_button_set_adjustment
	(GTK_SPIN_BUTTON (spin), GTK_ADJUSTMENT (adjustment));
    gtop_properties_adjustment_new (client, "/global/update_details",
				    GTK_ADJUSTMENT (adjustment));
    gtk_table_attach_defaults (GTK_TABLE (table), spin, 1, 2, 4, 5);

    label = gtk_label_new (_("ms"));
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_table_attach_defaults (GTK_TABLE (table), label, 2, 3, 4, 5);

    gtk_container_add (GTK_CONTAINER (frame), table);
    hb = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
    gtk_box_pack_start (GTK_BOX (hb), frame, TRUE, TRUE, 0);

    frame = gtk_frame_new (_("MDI Mode"));
    table = gtk_table_new (3, 4, FALSE);
    gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD);

    button = gtk_radio_button_new_with_label (NULL, _("Notebook"));
    gtop_properties_radio_button_new (client, "/global/mdi_mode",
				      GTK_RADIO_BUTTON (button));

    gtk_table_attach (GTK_TABLE (table), button, 1, 2, 0, 1,
		      GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);

    group = gtk_radio_button_group (GTK_RADIO_BUTTON (button));
    button = gtk_radio_button_new_with_label (group, _("Toplevel"));
    gtop_properties_radio_button_new (client, "/global/mdi_mode",
				      GTK_RADIO_BUTTON (button));

    gtk_table_attach (GTK_TABLE (table), button, 1, 2, 1, 2,
		      GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);

    group = gtk_radio_button_group (GTK_RADIO_BUTTON (button));
    button = gtk_radio_button_new_with_label (group, _("Modal"));
    gtop_properties_radio_button_new (client, "/global/mdi_mode",
				      GTK_RADIO_BUTTON (button));

    gtk_table_attach (GTK_TABLE (table), button, 1, 2, 2, 3,
		      GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);

    gtk_container_add (GTK_CONTAINER (frame), table);
    gtk_box_pack_start (GTK_BOX (hb), frame, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX (vb), hb, FALSE, TRUE, 0);

    return vb;
}
