#include <config.h>
#include <gnome.h>
#include <global.h>

#include <properties.h>
#include <libgnomeui/gnome-window-icon.h>

#include <glib.h>

typedef struct _CallbackData		CallbackData;
typedef struct _FontCallbackData	FontCallbackData;

struct _CallbackData {
    GConfClient *client;

    gchar *config_key;
};

struct _FontCallbackData {
    CallbackData *cb_data;

    GtkWidget *fsd;
    GtkWidget *entry;
};

static void
toggle_button_cb (GtkWidget *widget, CallbackData *cb_data)
{
    GConfValue *value;

    value = gtop_properties_check_button_get (GTK_CHECK_BUTTON (widget),
					      GCONF_VALUE_BOOL);
    if (value) {
	GConfError *error = NULL;

	gconf_client_set (cb_data->client, cb_data->config_key, value, &error);
	if (error) {
	    g_warning (G_STRLOC ": %s", error->str);
	    gconf_error_destroy (error);
	    error = NULL;
	}

	gconf_value_destroy (value);
    }
}

static void
adjustment_changed_cb (GtkWidget *widget, CallbackData *cb_data)
{
    GConfValue *value;

    value = gtop_properties_adjustment_get (GTK_ADJUSTMENT (widget),
					    GCONF_VALUE_FLOAT);
    if (value) {
	GConfError *error = NULL;

	gconf_client_set (cb_data->client, cb_data->config_key, value, &error);
	if (error) {
	    g_warning (G_STRLOC ": %s", error->str);
	    gconf_error_destroy (error);
	    error = NULL;
	}

	gconf_value_destroy (value);
    }
}

static void
radio_button_cb (GtkWidget *widget, CallbackData *cb_data)
{
    GConfValue *value;

    value = gtop_properties_radio_button_get (GTK_RADIO_BUTTON (widget),
					      GCONF_VALUE_INT);
    if (value) {
	GConfError *error = NULL;

	gconf_client_set (cb_data->client, cb_data->config_key, value, &error);
	if (error) {
	    g_warning (G_STRLOC ": %s", error->str);
	    gconf_error_destroy (error);
	    error = NULL;
	}

	gconf_value_destroy (value);
    }
}

static void
destroy_cb_data (CallbackData *cb_data)
{
    gtk_object_unref (GTK_OBJECT (cb_data->client));

    g_free (cb_data->config_key);
    g_free (cb_data);
}

static void
destroy_font_cb_data (FontCallbackData *fcb_data)
{
    destroy_cb_data (fcb_data->cb_data);

    if (fcb_data->fsd)
	gtk_widget_unref (fcb_data->fsd);
    if (fcb_data->entry)
	gtk_widget_unref (fcb_data->entry);

    g_free (fcb_data);
}

void
gtop_properties_check_button_set (GtkCheckButton *button,
				  GConfValue *value)
{
    g_return_if_fail (button != NULL);
    g_return_if_fail (GTK_IS_CHECK_BUTTON (button));
    g_return_if_fail (value != NULL);
    g_return_if_fail ((value->type == GCONF_VALUE_BOOL) ||
		      (value->type == GCONF_VALUE_INT));

    switch (value->type) {
    case GCONF_VALUE_BOOL:
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (button),
				     gconf_value_bool (value));
	break;
    case GCONF_VALUE_INT:
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (button),
				     gconf_value_int (value) ?
				     TRUE : FALSE);
	break;
    default:
	g_assert_not_reached ();
    }
}

GConfValue *
gtop_properties_check_button_get (GtkCheckButton *button,
				  GConfValueType type)
{
    GConfValue *retval = NULL;
    gboolean state;

    g_return_val_if_fail (button != NULL, NULL);
    g_return_val_if_fail (GTK_IS_CHECK_BUTTON (button), NULL);
    g_return_val_if_fail ((type == GCONF_VALUE_BOOL) ||
			  (type == GCONF_VALUE_INT), NULL);

    state = GTK_TOGGLE_BUTTON (button)->active;

    retval = gconf_value_new (type);
    switch (type) {
    case GCONF_VALUE_BOOL:
	gconf_value_set_bool (retval, state);
	break;
    case GCONF_VALUE_INT:
	gconf_value_set_int (retval, state);
	break;
    default:
	g_assert_not_reached ();
    }

    return retval;
}

void
gtop_properties_check_button_new (GConfClient *client, const gchar *config_key,
				  GtkWidget *button)
{
    CallbackData *cb_data;
    GConfError *error = NULL;
    GConfValue *value;

    g_return_if_fail (client != NULL);
    g_return_if_fail (GCONF_IS_CLIENT (client));
    g_return_if_fail (config_key != NULL);
    g_return_if_fail (button != NULL);
    g_return_if_fail (GTK_IS_CHECK_BUTTON (button));

    cb_data = g_new0 (CallbackData, 1);
    cb_data->client = client;
    cb_data->config_key = g_strdup_printf ("%s%s", GTOP_GCONF_DIR, config_key);
    gtk_object_ref (GTK_OBJECT (client));

    gtk_signal_connect_full (GTK_OBJECT (button), "toggled",
			     toggle_button_cb, NULL, cb_data,
			     (GtkDestroyNotify) destroy_cb_data,
			     FALSE, FALSE);

    value = gconf_client_get (client, cb_data->config_key, &error);
    if (error) {
	g_warning (G_STRLOC ": %s", error->str);
	gconf_error_destroy (error);
	error = NULL;
    }

    if (value) {
	gtop_properties_check_button_set (GTK_CHECK_BUTTON (button), value);
	gconf_value_destroy (value);
    }
}

void
gtop_properties_adjustment_new (GConfClient *client,
				const gchar *config_key,
				GtkAdjustment *adjustment)
{
    CallbackData *cb_data;
    GConfError *error = NULL;
    GConfValue *value;

    g_return_if_fail (client != NULL);
    g_return_if_fail (GCONF_IS_CLIENT (client));
    g_return_if_fail (config_key != NULL);
    g_return_if_fail (adjustment != NULL);
    g_return_if_fail (GTK_IS_ADJUSTMENT (adjustment));

    cb_data = g_new0 (CallbackData, 1);
    cb_data->client = client;
    cb_data->config_key = g_strdup_printf ("%s%s", GTOP_GCONF_DIR, config_key);
    gtk_object_ref (GTK_OBJECT (client));

    gtk_signal_connect_full (GTK_OBJECT (adjustment), "value_changed",
			     adjustment_changed_cb, NULL, cb_data,
			     (GtkDestroyNotify) destroy_cb_data,
			     FALSE, FALSE);

    value = gconf_client_get (client, cb_data->config_key, &error);
    if (error) {
	g_warning (G_STRLOC ": %s", error->str);
	gconf_error_destroy (error);
	error = NULL;
    }

    if (value) {
	gtop_properties_adjustment_set (adjustment, value);
	gconf_value_destroy (value);
    }
}

GConfValue *
gtop_properties_adjustment_get (GtkAdjustment *adjustment,
				GConfValueType type)
{
    GConfValue *retval = NULL;

    g_return_val_if_fail (adjustment != NULL, NULL);
    g_return_val_if_fail (GTK_IS_ADJUSTMENT (adjustment), NULL);
    g_return_val_if_fail ((type == GCONF_VALUE_INT) ||
			  (type == GCONF_VALUE_FLOAT), NULL);

    retval = gconf_value_new (type);
    switch (type) {
    case GCONF_VALUE_INT:
	gconf_value_set_int (retval, adjustment->value);
	break;
    case GCONF_VALUE_FLOAT:
	gconf_value_set_float (retval, adjustment->value);
	break;
    default:
	g_assert_not_reached ();
    }

    return retval;
}

void
gtop_properties_adjustment_set (GtkAdjustment *adjustment,
				GConfValue *value)
{
    float new_value;

    g_return_if_fail (adjustment != NULL);
    g_return_if_fail (GTK_IS_ADJUSTMENT (adjustment));
    g_return_if_fail (value != NULL);
    g_return_if_fail ((value->type == GCONF_VALUE_INT) ||
		      (value->type == GCONF_VALUE_FLOAT));

    new_value = adjustment->value;

    switch (value->type) {
    case GCONF_VALUE_INT:
	new_value = gconf_value_int (value);
	break;
    case GCONF_VALUE_FLOAT:
	new_value = gconf_value_float (value);
	break;
    default:
	g_assert_not_reached ();
    }

    if ((new_value < adjustment->lower) || (new_value >= adjustment->upper))
	g_warning (G_STRLOC ": adjustment value %f not between %f and %f",
		   new_value, adjustment->lower, adjustment->upper);
    else
	gtk_adjustment_set_value (adjustment, new_value);
}

void
gtop_properties_radio_button_new (GConfClient *client,
				  const gchar *config_key,
				  GtkRadioButton *button)
{
    CallbackData *cb_data;
    GConfError *error = NULL;
    GConfValue *value;

    g_return_if_fail (client != NULL);
    g_return_if_fail (GCONF_IS_CLIENT (client));
    g_return_if_fail (config_key != NULL);
    g_return_if_fail (button != NULL);
    g_return_if_fail (GTK_IS_RADIO_BUTTON (button));

    cb_data = g_new0 (CallbackData, 1);
    cb_data->client = client;
    cb_data->config_key = g_strdup_printf ("%s%s", GTOP_GCONF_DIR, config_key);
    gtk_object_ref (GTK_OBJECT (client));

    gtk_signal_connect_full (GTK_OBJECT (button), "toggled",
			     radio_button_cb, NULL, cb_data,
			     (GtkDestroyNotify) destroy_cb_data,
			     FALSE, FALSE);

    value = gconf_client_get (client, cb_data->config_key, &error);
    if (error) {
	g_warning (G_STRLOC ": %s", error->str);
	gconf_error_destroy (error);
	error = NULL;
    }

    if (value) {
	gtop_properties_radio_button_set (button, value);
	gconf_value_destroy (value);
    }
}

void
gtop_properties_radio_button_set (GtkRadioButton *radio,
				  GConfValue *value)
{
    gint i, j = 0;
    GSList *list;

    g_return_if_fail (radio != NULL);
    g_return_if_fail (GTK_IS_RADIO_BUTTON (radio));
    g_return_if_fail (value != NULL);
    g_return_if_fail ((value->type == GCONF_VALUE_BOOL) ||
		      (value->type == GCONF_VALUE_INT));

    switch (value->type) {
    case GCONF_VALUE_BOOL:
	j = gconf_value_bool (value);
	break;
    case GCONF_VALUE_INT:
	j = gconf_value_int (value);
	break;
    default:
	g_assert_not_reached();
    }

    for (list = radio->group, i = 0 ;
	 i != j && list != NULL;
	 i++, list = list->next)
	;
    if (list)
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (list->data), TRUE);
}

GConfValue *
gtop_properties_radio_button_get (GtkRadioButton *radio,
				  GConfValueType type)
{
    GConfValue *retval = NULL;
    GSList *group;
    int i = 0;

    g_return_val_if_fail (radio != NULL, NULL);
    g_return_val_if_fail (GTK_IS_RADIO_BUTTON (radio), NULL);
    g_return_val_if_fail ((type == GCONF_VALUE_BOOL) ||
			  (type == GCONF_VALUE_INT), NULL);

    retval = gconf_value_new (type);
    for (i = 0, group = radio->group; group != NULL; i++, group = group->next) {
	if (GTK_TOGGLE_BUTTON (group->data)->active) {
	    if (type == GCONF_VALUE_BOOL) {
		if (i > 1)
		    g_warning (G_STRLOC ": more then two radio buttons used "
			       "with a boolean");
		else
		    gconf_value_set_bool (retval, i);
	    } else {
		gconf_value_set_int (retval, i);
	    }
	    break;
	}
    }

    return retval;
}

static void
font_select_ok_cb (GtkWidget *widget, FontCallbackData *fcb_data)
{
    GtkFontSelectionDialog *fsd;
    gchar *font_name;
    GdkFont *font;

    g_return_if_fail (fcb_data != NULL);
    g_return_if_fail (fcb_data->fsd != NULL);
    g_return_if_fail (GTK_IS_FONT_SELECTION_DIALOG (fcb_data->fsd));
    g_return_if_fail (fcb_data->entry != NULL);
    g_return_if_fail (GTK_IS_ENTRY (fcb_data->entry));

    fsd = GTK_FONT_SELECTION_DIALOG (fcb_data->fsd);

    font_name = gtk_font_selection_dialog_get_font_name (fsd);

    if (font_name == NULL) {
	gdk_beep ();
	return;
    }

    /* Make sure it is a valid font. */

    font = gdk_font_load (font_name);
	
    if (!font) {
	g_free (font_name);
	gdk_beep ();
	return;
    }

    gtk_entry_set_text (GTK_ENTRY (fcb_data->entry), font_name);

    g_free (font_name);
    gdk_font_unref (font);

    gtk_widget_hide (fcb_data->fsd);
}

static void
font_entry_activate_cb (GtkWidget *widget, FontCallbackData *fcb_data)
{
    GConfError *error = NULL;
    GConfValue *value;
    gchar *font_name;
    GdkFont *font;

    g_return_if_fail (fcb_data != NULL);
    g_return_if_fail (fcb_data->entry != NULL);
    g_return_if_fail (GTK_IS_ENTRY (fcb_data->entry));

    font_name = g_strdup (gtk_entry_get_text (GTK_ENTRY (fcb_data->entry)));

    /* Make sure it is a valid font. */

    font = gdk_font_load (font_name);
	
    if (!font) {
	g_free (font_name);
	gdk_beep ();
	return;
    }

    value = gconf_value_new (GCONF_VALUE_STRING);
    gconf_value_set_string (value, font_name);

    gconf_client_set (fcb_data->cb_data->client,
		      fcb_data->cb_data->config_key,
		      value, &error);
    if (error) {
	g_warning (G_STRLOC ": %s", error->str);
	gconf_error_destroy (error);
	error = NULL;
    }

    gconf_value_destroy (value);

    g_free (font_name);
    gdk_font_unref (font);
}

static void
destroy_font_entry (GtkWidget *widget, FontCallbackData *fcb_data)
{
    g_return_if_fail (fcb_data != NULL);
    g_return_if_fail (fcb_data->fsd != NULL);
    g_return_if_fail (GTK_IS_FONT_SELECTION_DIALOG (fcb_data->fsd));
    g_return_if_fail (fcb_data->entry != NULL);
    g_return_if_fail (GTK_IS_ENTRY (fcb_data->entry));

    gtk_widget_unref (fcb_data->fsd);
    gtk_widget_unref (fcb_data->entry);

    destroy_font_cb_data (fcb_data);
}

GtkWidget *
gtop_properties_font_entry_new (GConfClient *client,
				const gchar *label,
				const gchar *config_key)
{
    FontCallbackData *fcb_data;
    GConfError *error = NULL;
    GConfValue *value;
    GtkWidget *table, *entry, *button;
    GtkFontSelectionDialog *fsd;

    g_return_val_if_fail (client != NULL, NULL);
    g_return_val_if_fail (GCONF_IS_CLIENT (client), NULL);
    g_return_val_if_fail (config_key != NULL, NULL);

    table = gtk_table_new (1, 4, TRUE);
    gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD << 2);

    entry = gtk_entry_new ();

    gtk_table_attach_defaults (GTK_TABLE (table), entry, 0, 3, 0, 1);

    button = gtk_button_new_with_label (_("Select"));

    fsd = GTK_FONT_SELECTION_DIALOG (gtk_font_selection_dialog_new (label));
    gnome_window_icon_set_from_default (GTK_WINDOW (fsd));
 
    fcb_data = g_new0 (FontCallbackData, 1);
    fcb_data->cb_data = g_new0 (CallbackData, 1);
    fcb_data->cb_data->client = client;
    fcb_data->cb_data->config_key = g_strdup_printf
	("%s%s", GTOP_GCONF_DIR, config_key);

    fcb_data->entry = entry;
    fcb_data->fsd = GTK_WIDGET (fsd);

    /* The `fcb_data' structure takes over ownership of the dialog. */
    gtk_object_sink (GTK_OBJECT (fcb_data->fsd));
    gtk_widget_ref (fcb_data->fsd);

    gtk_widget_ref (fcb_data->entry);
    gtk_object_ref (GTK_OBJECT (fcb_data->cb_data->client));

    gtk_signal_connect (GTK_OBJECT (entry), "activate",
			font_entry_activate_cb, fcb_data);

    gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			       gtk_widget_show, GTK_OBJECT (fsd));

    gtk_signal_connect (GTK_OBJECT (fsd->ok_button),
			"pressed", font_select_ok_cb, fcb_data);

    gtk_signal_connect_object (GTK_OBJECT (fsd->cancel_button),
			       "pressed", gtk_widget_hide,
			       GTK_OBJECT (fsd));

    gtk_signal_connect (GTK_OBJECT (table), "destroy",
			destroy_font_entry, fcb_data);

    gtk_table_attach_defaults (GTK_TABLE (table), button, 3, 4, 0, 1);

    value = gconf_client_get (client, fcb_data->cb_data->config_key, &error);
    if (error) {
	g_warning (G_STRLOC ": %s", error->str);
	gconf_error_destroy (error);
	error = NULL;
    }

    if (value) {
	gtk_entry_set_text (GTK_ENTRY (entry), gconf_value_string (value));
	gconf_value_destroy (value);
    }

    return table;
}

static void
color_changed_cb (GnomeColorPicker *cp, gint r, gint g,
		  gint b, gint a, CallbackData *cb_data)
{
    GConfValue *value = NULL;

    g_return_if_fail (cp != NULL);
    g_return_if_fail (GNOME_IS_COLOR_PICKER (cp));	
    g_return_if_fail (cb_data != NULL);

    value = gtop_properties_color_entry_get (cp, GCONF_VALUE_STRING);

    if (value) {
	GConfError *error = NULL;

	gconf_client_set (cb_data->client, cb_data->config_key, value, &error);
	if (error) {
	    g_warning (G_STRLOC ": %s", error->str);
	    gconf_error_destroy (error);
	    error = NULL;
	}

	gconf_value_destroy (value);
    }
}

void
gtop_properties_color_entry_new (GConfClient *client,
				 const gchar *config_key,
				 GnomeColorPicker *picker)
{
    CallbackData *cb_data;
    GConfError *error = NULL;
    GConfValue *value;

    g_return_if_fail (client != NULL);
    g_return_if_fail (GCONF_IS_CLIENT (client));
    g_return_if_fail (config_key != NULL);
    g_return_if_fail (picker != NULL);
    g_return_if_fail (GNOME_IS_COLOR_PICKER (picker));

    cb_data = g_new0 (CallbackData, 1);
    cb_data->client = client;
    cb_data->config_key = g_strdup_printf ("%s%s", GTOP_GCONF_DIR, config_key);
    gtk_object_ref (GTK_OBJECT (client));

    gtk_signal_connect_full (GTK_OBJECT (picker), "color_set",
			     color_changed_cb, NULL, cb_data,
			     (GtkDestroyNotify) destroy_cb_data,
			     FALSE, FALSE);

    value = gconf_client_get (client, cb_data->config_key, &error);
    if (error) {
	g_warning (G_STRLOC ": %s", error->str);
	gconf_error_destroy (error);
	error = NULL;
    }

    if (value) {
	gtop_properties_color_entry_set (picker, value);
	gconf_value_destroy (value);
    }
}

void
gtop_properties_color_entry_set (GnomeColorPicker *picker,
				 GConfValue *value)
{
    gushort r, g, b, a;
    gchar *color;

    g_return_if_fail (picker != NULL);
    g_return_if_fail (GNOME_IS_COLOR_PICKER (picker));
    g_return_if_fail (value != NULL);
    g_return_if_fail (value->type == GCONF_VALUE_STRING);

    color = g_strndup (gconf_value_string (value), 17);
    a = strtol (color  + 13, (char **)NULL, 16);
    *(color +13) = '\0';
    b = strtol (color  + 9, (char **)NULL, 16);
    *(color +9) = '\0';
    g = strtol (color  + 5, (char **)NULL, 16);
    *(color +5) = '\0';
    r = strtol (color  + 1, (char **)NULL, 16);
    gnome_color_picker_set_i16 (picker, r, g, b, a);
    g_free (color);
}

GConfValue *
gtop_properties_color_entry_get (GnomeColorPicker *picker,
				 GConfValueType type)
{
    GConfValue *retval = NULL;
    gushort r, g, b, a;
    gchar color[18];

    g_return_val_if_fail (picker != NULL, NULL);
    g_return_val_if_fail (GNOME_IS_COLOR_PICKER (picker), NULL);
    g_return_val_if_fail (type == GCONF_VALUE_STRING, NULL);

    retval = gconf_value_new (type);
    switch (type) {
    case GCONF_VALUE_STRING:
	gnome_color_picker_get_i16 (picker, &r, &g, &b, &a);
	g_snprintf (color, 18, "#%04X%04X%04X%04X", r, g, b, a);
	gconf_value_set_string (retval, color);
	break;
    default:
	g_assert_not_reached ();
    }

    return retval;
}

