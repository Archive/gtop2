#include <gtop-libgtop-client.h>
#include <gtop-procbar-cpu.h>

#include <gnome.h>
#include <libgnomeui/gnome-window-icon.h>

#include <pong/pong.h>
#include <pong/pong-glade.h>

#include <locale.h>

#define GTOP_GCONF_DIR "/apps/gtop2/foo"

static PongXML *config = NULL;

static void
file_exit (void)
{
    gtk_main_quit ();
}

static void
settings_properties (void)
{
    pong_xml_show_dialog (config);
}

GnomeUIInfo fileMenu [] = {
    GNOMEUIINFO_MENU_EXIT_ITEM (file_exit, NULL),
    GNOMEUIINFO_END
};

GnomeUIInfo settingsMenu [] = {
    GNOMEUIINFO_MENU_PREFERENCES_ITEM (settings_properties, NULL),
    GNOMEUIINFO_END
};
  
GnomeUIInfo mainMenu [] = {
    GNOMEUIINFO_MENU_FILE_TREE (fileMenu),
    GNOMEUIINFO_MENU_SETTINGS_TREE (settingsMenu),
    GNOMEUIINFO_END
};

int
main (int argc, char *argv[])
{
    GtkWidget *app, *vbox, *pb_cpu;
    GTopLibGTopClient *glc;
    GConfClient *client;
    glibtop *server;

    setlocale (LC_ALL, "");
    bindtextdomain (PACKAGE, GNOMELOCALEDIR);
    textdomain (PACKAGE);

    gnome_init ("gtop2", VERSION, argc, argv);
    gnome_window_icon_set_default_from_file (GNOME_ICONDIR "/gnome-gtop.png");

    gconf_init (argc, argv, NULL);
    client = gconf_client_get_default ();

    server = NULL;

    gconf_client_add_dir (client, GTOP_GCONF_DIR, GCONF_CLIENT_PRELOAD_NONE,
			  NULL);

    /* The main() function takes over the floating object; the code that
       "owns" the object should do this, as with any Gtk object.
       Read about refcounting and destruction at developer.gnome.org/doc/GGAD/ */
    gtk_object_ref (GTK_OBJECT (client));
    gtk_object_sink (GTK_OBJECT (client));


    /* Initialize Pong. */
    if (!pong_init ())
	g_error ("PonG init failed!");
    if (!pong_glade_init ())
	g_error ("PonG glade init failed!");

    app = gnome_app_new ("gtop", "GTop");

    gnome_app_create_menus (GNOME_APP (app), mainMenu);

    gtk_widget_set_usize (app, 800, 600);

    glc = GTOP_LIBGTOP_CLIENT (gtop_libgtop_client_new (client, server));

    vbox = gtk_vbox_new (FALSE, GNOME_PAD);

    pb_cpu = gtop_procbar_cpu_new (glc, client, GTOP_GCONF_DIR "/summary/colors/cpu");

    gtk_widget_set_usize (pb_cpu, 800, 40);

    gtk_box_pack_start (GTK_BOX (vbox), pb_cpu, FALSE, FALSE, GNOME_PAD_SMALL);

    gnome_app_set_contents (GNOME_APP (app), vbox);
    gtk_widget_show_all (app);

    gtop_libgtop_client_start (glc);

    config = pong_xml_new ("gtop2.pong", FALSE, NULL);
    if (!config)
	g_error ("Can't load pong file");

    gtk_main ();

    gconf_client_remove_dir (client, GTOP_GCONF_DIR, NULL);

    gtk_object_unref (GTK_OBJECT (client));

    return 0;
}
